"""
CSC148, Winter 2019
Assignment 1

This code is provided solely for the personal and private use of
students taking the CSC148 course at the University of Toronto.
Copying for purposes other than this use is expressly prohibited.
All forms of distribution of this code, whether as given or with
any changes, are expressly prohibited.

All of the files in this directory and all subdirectories are:
Copyright (c) 2019 Bogdan Simion, Diane Horton, Jacqueline Smith
"""
import datetime
from math import ceil
from typing import Optional, Tuple
from bill import Bill
from call import Call


# Constants for the month-to-month contract monthly fee and term deposit
MTM_MONTHLY_FEE = 50.00
TERM_MONTHLY_FEE = 20.00
TERM_DEPOSIT = 300.00

# Constants for the included minutes and SMSs in the term contracts (per month)
TERM_MINS = 100

# Cost per minute and per SMS in the month-to-month contract
MTM_MINS_COST = 0.05

# Cost per minute and per SMS in the term contract
TERM_MINS_COST = 0.1

# Cost per minute and per SMS in the prepaid contract
PREPAID_MINS_COST = 0.025


class Contract:
    """ A contract for a phone line

    This is an abstract class. Only subclasses should be instantiated.

    === Public Attributes ===
    start:
         starting date for the contract
    bill:
         bill for this contract for the last month of call records loaded from
         the input dataset
    """
    start: datetime.datetime
    bill: Optional[Bill]

    def __init__(self, start: datetime.date) -> None:
        """ Create a new Contract with the <start> date, starts as inactive
        """
        self.start = start
        self.bill = None

    def new_month(self, month: int, year: int, bill: Bill) -> None:
        """ Advance to a new month in the contract, corresponding to <month> and
        <year>. This may be the first month of the contract.
        Store the <bill> argument in this contract and set the appropriate rate
        per minute and fixed cost.
        """
        raise NotImplementedError

    def bill_call(self, call: Call) -> None:
        """ Add the <call> to the bill.

        Precondition:
        - a bill has already been created for the month+year when the <call>
        was made. In other words, you can safely assume that self.bill has been
        already advanced to the right month+year.
        """
        call_dur = ceil(call.duration / 60.0)
        self.bill.add_billed_minutes(call_dur)

    def cancel_contract(self) -> float:
        """ Return the amount owed in order to close the phone line associated
        with this contract.

        Precondition:
        - a bill has already been created for the month+year when this contract
        is being cancelled. In other words, you can safely assume that self.bill
        exists for the right month+year when the cancelation is requested.
        """
        self.start = None
        return self.bill.get_cost()


class MTMContract(Contract):
    """ A contract for a phone line to be used month to month

    === Public Attributes ===
    start:
         starting date for the contract
    bill:
         bill for this contract for the last month of call records loaded from
         the input dataset
    """
    start: datetime.date
    bill: Optional[Bill]
    # DONE: Implement this class (3)

    def __init__(self, start: datetime.date) -> None:
        """ Create a new month to month contract starting on <start>
        This contract starts inactive
        """
        self.start = start
        self.bill = None

    def new_month(self, month: int, year: int, bill: Bill) -> None:
        """ Advance to a new month in the contract, corresponding to <month> and
        <year>. This may be the first month of the contract.
        Store the <bill> argument in this contract and set the appropriate rate
        per minute and fixed cost.
        """
        self.bill = bill
        self.bill.set_rates("MTM", MTM_MINS_COST)
        self.bill.add_fixed_cost(MTM_MONTHLY_FEE)

    def bill_call(self, call: Call) -> None:
        """ Add the <call> to the bill.

        Precondition:
        - a bill has already been created for the month+year when the <call>
        was made. In other words, you can safely assume that self.bill has been
        already advanced to the right month+year.
        """
        call_dur = ceil(call.duration / 60.0)
        self.bill.add_billed_minutes(call_dur)

    def cancel_contract(self) -> float:
        """ Return the amount owed in order to close the phone line associated
        with this contract.

        Precondition:
        - a bill has already been created for the month+year when this contract
        is being cancelled. In other words, you can safely assume that self.bill
        exists for the right month+year when the cancelation is requested.
        """
        return self.bill.get_cost()


class TermContract(Contract):
    """ A contract for a phone line with given start and end date

    === Public Attributes ===
    start:
         starting date for the contract
    end:
         ending date for the contract
    bill:
         bill for this contract for the last month of call records loaded from
         the input dataset

    === Private Attributes ===
    _now:
         the current date, only used to hold month and year
    """
    start: datetime.date
    end: datetime.date
    bill: Optional[Bill]
    _now: Tuple[int, int]
    # DONE: Implement this class (3)

    def __init__(self, start: datetime.date, end: datetime.date) -> None:
        self.start = start
        self.end = end
        self._now = (start.month, start.year)
        self.bill = None

    def new_month(self, month: int, year: int, bill: Bill) -> None:
        self.bill = bill
        self.bill.set_rates("TERM", TERM_MINS_COST)

        # Update _now
        self._now = (month, year)

        # Check for first month for deposit
        if (month, year) == (self.start.month, self.start.year):
            self.bill.add_fixed_cost(TERM_DEPOSIT)

        self.bill.add_fixed_cost(TERM_MONTHLY_FEE)

    def bill_call(self, call: Call) -> None:
        """ Add the <call> to the bill.

        Precondition:
        - a bill has already been created for the month+year when the <call>
        was made. In other words, you can safely assume that self.bill has been
        already advanced to the right month+year.
        """

        call_dur = ceil(call.duration / 60.0)
        free_used = self.bill.get_summary()['free_mins']
        free_avail = TERM_MINS - free_used

        # Check if free minutes
        if free_avail > 0:
            if call_dur > free_avail:  # If duration was more than free
                self.bill.add_free_minutes(free_avail)
                self.bill.add_billed_minutes(call_dur - free_avail)
            else:  # All Free
                self.bill.add_free_minutes(call_dur)
        else:  # All Billed
            self.bill.add_billed_minutes(call_dur)

    def cancel_contract(self) -> float:
        """ Return the amount owed in order to close the phone line associated
        with this contract.

        Precondition:
        - a bill has already been created for the month+year when this contract
        is being cancelled. In other words, you can safely assume that self.bill
        exists for the right month+year when the cancelation is requested.
        """
        self.start = None
        datenow = datetime.date(self._now[1], self._now[0], 1)

        if datenow > self.end:  # Check for deposit obtaining
            return self.bill.get_cost() - TERM_DEPOSIT
        return self.bill.get_cost()


class PrepaidContract(Contract):
    """ A contract for a phone line with a prepaid balance

    === Public Attributes ===
    start:
         starting date for the contract
    bill:
         bill for this contract for the last month of call records loaded from
         the input dataset
    === Private Attributes ===
    _bal:
         The balance that was initially passed in as a parameter
    """
    start: datetime.datetime
    bill: Optional[Bill]
    # DONE: Implement this class (3)

    def __init__(self, start: datetime.date, balance: float) -> None:
        """ Create a new PrepaidContract with the <start> date,
        starts as active with <bal> in credit
        """
        self.start = start
        self.bill = Bill()
        self.new_month(self.start.month, self.start.year, self.bill)
        self.bill.add_fixed_cost(-balance)  # -balance as credit notation

    def new_month(self, month: int, year: int, bill: Bill) -> None:
        """ Advance to a new month in the contract, corresponding to <month> and
        <year>. This may be the first month of the contract.
        Store the <bill> argument in this contract and set the appropriate rate
        per minute and fixed cost.
        """
        # Hold balance over from months
        bal = self.bill.get_cost()

        self.bill = bill
        self.bill.set_rates("PREPAID", PREPAID_MINS_COST)

        # Balance Logic
        if bal > -10 and (month, year) != (self.start.month, self.start.year):
            bal -= 25

        self.bill.add_fixed_cost(bal)

    def bill_call(self, call: Call) -> None:
        """ Add the <call> to the bill.

        Precondition:
        - a bill has already been created for the month+year when the <call>
        was made. In other words, you can safely assume that self.bill has been
        already advanced to the right month+year.
        """
        call_dur = ceil(call.duration / 60.0)
        self.bill.add_billed_minutes(call_dur)

    def cancel_contract(self) -> float:
        """ Return the amount owed in order to close the phone line associated
        with this contract.

        Precondition:
        - a bill has already been created for the month+year when this contract
        is being cancelled. In other words, you can safely assume that self.bill
        exists for the right month+year when the cancelation is requested.
        """
        self.start = None
        return self.bill.get_cost()


if __name__ == '__main__':
    import python_ta
    python_ta.check_all(config={
        'allowed-import-modules': [
            'python_ta', 'typing', 'datetime', 'bill', 'call', 'math'
        ],
        'disable': ['R0902', 'R0913'],
        'generated-members': 'pygame.*'
    })
